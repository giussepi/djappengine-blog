# -*- coding: utf-8 -*-
""" comments's indexing settings  """

from django.contrib.comments.models import Comment

FIELD_INDEXES = {
    Comment: {'indexed': ['object_pk']}
}
