""" test_app's urls """

from django.conf.urls.defaults import *
from django.contrib import admin
from registration.forms import RegistrationFormUniqueEmail
admin.autodiscover()


handler500 = 'djangotoolbox.errorviews.server_error'

urlpatterns = patterns('',
    ('^_ah/warmup$', 'djangoappengine.views.warmup'),
    url('^', include('blog.urls')),
    url(r'^accounts/register/$', 'registration.views.register',
        {'form_class': RegistrationFormUniqueEmail,
         'backend': 'registration.backends.default.DefaultBackend'},       
         name='registration_register'),    
    url(r'^accounts/', include('registration.backends.default.urls') ),
    url(r'^comments/', include('django.contrib.comments.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
