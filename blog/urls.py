# -*- coding: utf-8 -*-
""" blog's urls """

from django.conf.urls.defaults import *

urlpatterns = patterns(
    'blog.views',
    url(r'^$', 'home', name='blog_home'),
    url(r'^profile/$', 'author_edit_profile',
        name='blog_author_edit_profile'),
    url(r'^profile/(?P<username>[-_\w]+)/$', 'author_profile',
        name='blog_author_profile'),
    url(r'^entries/$', 'entries', name='blog_entries'),
    url(r'^add_new_entry/$', 'add_new_entry', name='blog_add_new_entry'),
    url((r'^entry/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})'
         '/(?P<slug>[-_\w]+)/$'), 'entry_detail', name='blog_entry_detail'),
    url(r'^entry_edit/(?P<id>\d+)/', 'entry_edit', name='blog_entry_edit'),
    url(r'^my_entries/$', 'my_entries', name='blog_my_entries'),
    url(r'^ajax_entry_delete/$', 'ajax_entry_delete', name='blog_ajax_entry_delete'),
)
