# -*- coding: utf-8 -*-
""" custom fields """

from django import forms
from django.utils.translation import ugettext_lazy as _
from blog.validators import validate_username


class UsernameField(forms.CharField):
    default_error_messages = {
        'invalid': _(u'"This value may contain only letters, numbers and \
        @/./+/-/_ characters."'),
    }
    default_validators = [validate_username]

    def __init__(self, *args, **kwargs):
        """ sets the initial help text """
        super(UsernameField, self).__init__(*args, **kwargs)
        self.help_text = _("Required. 30 characters or fewer. Letters, digits \
        and @/./+/-/_ only.")

    def clean(self, value):
        value = self.to_python(value).strip()
        return super(UsernameField, self).clean(value)
