# -*- coding: utf-8 -*-
""" blog's signals """

from django.contrib.auth.models import User
from django.db.models.signals import post_save


def user_created(sender, instance, created, **kwargs):
    """
    creates the profile Author only if the User has been just created
    """
    if created:
        from blog.models import Author
        Author(user=instance).save()


def setup_signals():
    """ links signals with models """
    post_save.connect(user_created, sender=User)
