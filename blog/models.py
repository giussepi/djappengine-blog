# -*- coding: utf-8 -*-
""" blog's models """

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
# from easy_thumbnails.fields import ThumbnailerField
import os


class Author(models.Model):
    """ Stores the authors """
    user = models.ForeignKey(User)
    date_joined = models.DateTimeField(auto_now_add=True)
    # docs says: ImageField is not supported by djangoappengine
    # avatar = ThumbnailerField(upload_to=os.path.join(
    #     settings.MEDIA_ROOT, 'avatars'), blank=True)

    def __unicode__(self):
        return u'%s' % self.user.get_full_name()

    @models.permalink
    def get_absolute_url(self):
        return ('blog_author_profile', (self.user.username, ))


class Category(models.Model):
    """ Stores the entries' categories """
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255)
    description = models.TextField(blank=True)

    class Meta:
        """Category's Meta class"""
        ordering = ['title']

    def __unicode__(self):
        return u'%s' % self.title


class Entry(models.Model):
    """ Stores the User's entries """
    title = models.CharField(max_length=255)
    text = models.TextField()
    excerpt = models.CharField(max_length=255)
    category = models.ForeignKey(Category)
    author = models.ForeignKey(Author)
    allow_comments = models.BooleanField(default=True)
    creation_date = models.DateField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    slug = models.SlugField(max_length=255)

    class Meta:
        """Entry's Meta class"""
        ordering = ['-last_update']

    @models.permalink
    def get_absolute_url(self):
        return ('blog_entry_detail', (), {
            'year': self.creation_date.strftime('%Y'),
            'month': self.creation_date.strftime('%m'),
            'day': self.creation_date.strftime('%d'),
            'slug': self.slug})

    def __unicode__(self):
        return u'%s' % self.title


from blog import signals
signals.setup_signals()
