# -*- coding: utf-8 -*-
""" blog's test """

from django.contrib.auth.models import User
from django.conf import settings
from django.core.urlresolvers import reverse
from django.test import TestCase
from blog.models import Author, Entry, Category
from blog.utils import unique_random_string, random_string_with_length, \
    unique_slug


class BaseOperations(TestCase):
    """ Basic Operations """
    @staticmethod
    def _create_user(active=False, superuser=False):
        """
        Creates a user (the signal creates its profile).
        Returns the user.
        """
        u = User.objects.create(
            username=unique_random_string(User, 'username', 5),
            is_active=active, is_staff=True, is_superuser=superuser)
        u.set_password('1')
        u.save()
        return u

    def _create_entry(self, author=None):
        """
        Creates and returns a new entry
        if not receives an author argument, it creates a new one.
        """
        title = random_string_with_length()
        new_entry = Entry.objects.create(
            title=title,
            text='a',
            excerpt='a',
            category=Category.objects.all()[0],
            author=author or self._create_user().get_profile(),
            allow_comments=True,
            slug=unique_slug(Entry, 'slug', title)
        )
        return new_entry


class OtherViews(BaseOperations):
    """ Tests of views that not belong to an specific model """
    def test_home(self):
        """ Verifies that the home page works properly """
        response = self.client.get(reverse('blog_home'))
        self.assertEqual(response.status_code, 200,
                         'home view: Http status code should be 200')
        self.assertTrue('latest_comments' in response.context,
                        'home view: The list of comments was not delivered \
                        to the template.')
        self.assertTrue('most_updated_entries' in response.context,
                        'home view: The entries\'s list was not \
                        delivered to the template.')


class TestsAuthor(BaseOperations):
    """ Author's Tests """

    def test_user_create_signal(self):
        """ Verifies that the user profile is created properly """
        user = self._create_user()
        self.assertEqual(User.objects.count(), 1,
                         'create_user signal: The user was not created.')
        self.assertEqual(Author.objects.count(), 1,
                         'create_user signal: The Author was not created.')
        author = Author.objects.all()[0]
        self.assertEqual(
            user.get_profile().id, author.id,
            'create_user signal: The user was not linked with the author.')

    def test_author_profile(self):
        """ Verifies that the views is shown properly """
        response = self.client.get(
            reverse('blog_author_profile', args=('asd',)))
        self.assertEqual(response.status_code, 404,
                         'author_profile view: A 404 should be raise.')
        user = self._create_user(True)
        response = self.client.get(
            reverse('blog_author_profile', args=(user.username,)))
        self.assertEqual(
            response.status_code, 200,
            'author_profile view: Http status code shoud be 200')

    def test_author_edit_profile(self):
        """
        Verifies that:
        1. The edit profile view just can be reached by loggedin user.
        2. The form works properly
        3. No new user or authors where created after the update.
        4. A redirection is made after form processing.
        5. The username is unique
        6. The email is unique
        """
        # 1
        response = self.client.get(reverse('blog_author_edit_profile'))
        self.assertRedirects(
            response,
            settings.LOGIN_URL + '?next=' + reverse(
                'blog_author_edit_profile'),
            msg_prefix='author_edit_profile view: The view was not \
            redirected properly.'
        )
        # 2
        user = self._create_user(True)
        result = self.client.login(
            username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        response = self.client.get(reverse('blog_author_edit_profile'))
        self.assertEqual(response.status_code, 200,
                         'author_edit_profile view: The view \
                         can\'t be reached by a logged in user.')
        response = self.client.post(reverse('blog_author_edit_profile'),
                                    {'id': user.id,
                                     'username': 'Marco',
                                     'first_name': 'Marco',
                                     'last_name': 'Polo',
                                     'email': 'marcopolo@mail.com'})
        updated_user = User.objects.get(id=user.id)
        self.assertTrue(updated_user.username == 'Marco' and
                        updated_user.first_name == 'Marco' and
                        updated_user.last_name == 'Polo' and
                        updated_user.email == 'marcopolo@mail.com',
                        'author_edit_profile view: The user wasn\'t \
                        updated properly.')
        # 3
        self.assertEqual(
            User.objects.count(), 1,
            'author_edit_profile view: A new user was created.')
        self.assertEqual(
            Author.objects.count(), 1,
            'author_edit_profile view: A new author was created.')

        # 4
        self.assertRedirects(
            response,
            reverse('blog_author_profile', args=(updated_user, )),
            msg_prefix='author_edit_profile view: The view wasn\'t not \
            redirected properly.'
        )
        # 5
        other_user = self._create_user()
        other_user.username = 'Rock'
        other_user.save()
        response = self.client.post(reverse('blog_author_edit_profile'),
                                    {'id': user.id,
                                     'username': 'Rock',
                                     'first_name': 'Marco',
                                     'last_name': 'Polo',
                                     'email': 'marcopolo@mail.com'})
        self.assertEqual(User.objects.filter(username='Rock').count(), 1,
                         'author_edit_profile view: The username is not \
                         unique anymore.')
        # 6
        other_user.email = 'rock@mail.com'
        other_user.save()
        response = self.client.post(reverse('blog_author_edit_profile'),
                                    {'id': user.id,
                                     'username': 'Marco',
                                     'first_name': 'Marco',
                                     'last_name': 'Polo',
                                     'email': 'rock@mail.com'})
        self.assertEqual(User.objects.filter(email='rock@mail.com').count(), 1,
                         'author_edit_profile view: The email is not \
                         unique anymore.')


class TestsEntry(BaseOperations):
    """ Entry's tests """

    def test_entries(self):
        """
        Verifies that the entries view works properly  and that all the
        entries are delivered to the template."""
        response = self.client.get(reverse('blog_entries'))
        self.assertEqual(response.status_code, 200,
                         'entries view: Http status code should be 200')
        self.assertTrue('entries' in response.context,
                        'entries view: The entries\'s list was not \
                        delivered to the template.')
        for i in xrange(10):
            self._create_entry()
        response = self.client.get(reverse('blog_entries'))
        self.assertEqual(response.context['entries'].count(), 10,
                         'entrie view: Not all the entries were \
                         delivered to the template.')

    def test_entry_detail(self):
        """
        Verifies that the entry_detail view works properly
        and that a 404 is raised when there's not an entry match. """
        entry = self._create_entry()
        response = self.client.get(entry.get_absolute_url())
        self.assertEqual(response.status_code, 200,
                         'entry_detail view: Http status code should be 200')
        self.assertTrue('entry' in response.context,
                        'entry_detail view: The entry object was not \
                        delivered to the template.')
        response = self.client.get(
            reverse('blog_entry_detail', args=('2012', '11', '01', 'my_slug')))
        self.assertEqual(response.status_code, 404,
                         'entry_detail view: A 404 shoud be raised.')

    def test_add_new_entry(self):
        """
        1.- Verifies that just logged in user can reach the view.
        2.- Verifies that the form works properly and the view is redirected
            properly.
        3.- Verifies that the slug is unique for the current date
        """
        # 1
        user = self._create_user(True)
        response = self.client.get(reverse('blog_add_new_entry'))
        self.assertRedirects(
            response,
            settings.LOGIN_URL + '?next=' + reverse('blog_add_new_entry'),
            msg_prefix='add_new_entry view: The view was not redirected \
            properly.'
        )
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        response = self.client.get(reverse('blog_add_new_entry'))
        self.assertEqual(response.status_code, 200,
                         'add_new_entry view: A logged in user \
                         could\'t reach the view.')
        # 2
        category = Category.objects.all()[0]
        response = self.client.post(reverse('blog_add_new_entry'),
                                    {'title': 'entry 1',
                                     'text': 'a',
                                     'excerpt': 'a',
                                     'category': category.id,
                                     'allow_comments': True,
                                     'slug': 'entry_1'})
        self.assertEqual(Entry.objects.all().count(), 1,
                         'add_new_entry view: The entry was not created.')
        entry = Entry.objects.all()[0]
        self.assertTrue(entry.title == 'entry 1' and entry.text == 'a' and
                        entry.excerpt == 'a' and entry.category == category and
                        entry.allow_comments is True and
                        entry.slug == 'entry_1',
                        'add_new_entry view: The entry was not created \
                        properly.')
        self.assertRedirects(
            response, entry.get_absolute_url(),
            msg_prefix='add_new_entry view: The view was not redirected \
            properly after saving the form.'
        )

        # 3
        response = self.client.post(reverse('blog_add_new_entry'),
                                    {'title': 'entry 1',
                                     'text': 'a',
                                     'excerpt': 'a',
                                     'category': category.id,
                                     'allow_comments': True,
                                     'slug': 'entry_1'})
        self.assertEqual(
            Entry.objects.all().count(), 1,
            'add_new_entry view: The entry shouldn\'t be created.')
        response = self.client.post(reverse('blog_add_new_entry'),
                                    {'title': 'entry 1',
                                     'text': 'a',
                                     'excerpt': 'a',
                                     'category': category.id,
                                     'allow_comments': True,
                                     'slug': 'entry_2'})
        self.assertEqual(
            Entry.objects.all().count(), 2,
            'add_new_entry view: The entry was not created.')

    def test_entry_edit(self):
        """
        1.- Verifies that just logged in user can reach the view.
        2.- Verifies that a 404 is raised when the entry's id is invalid.
        3.- Verifies that if a user tries to edit an entry that doesn't belong
            to him, he is redirected to the entry_detail view.
        4.- Verifies that the form works properly and the view is redirected
            properly.
        5.- Verifies that the slug is unique for the entry's creation date
        """
        # 1
        user = self._create_user(True)
        entry = self._create_entry(user.get_profile())
        response = self.client.get(
            reverse('blog_entry_edit', args=(entry.id,)))
        self.assertRedirects(
            response,
            settings.LOGIN_URL + '?next=' + reverse(
                'blog_entry_edit', args=(entry.id,)),
            msg_prefix='entry_edit view: The view was not redirected \
            properly.'
        )
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        response = self.client.get(
            reverse('blog_entry_edit', args=(entry.id,)))
        self.assertEqual(response.status_code, 200,
                         'entry_edit view: A logged in user \
                         could\'t reach the view.')

        # 2
        response = self.client.get(
            reverse('blog_entry_edit', args=(entry.id + 1,)))
        self.assertEqual(response.status_code, 404,
                         'entry_edit view: A 404 shoud be raised.')

        # 3
        other_entry = self._create_entry()
        response = self.client.get(
            reverse('blog_entry_edit', args=(other_entry.id, )))
        self.assertRedirects(response, other_entry.get_absolute_url(),
                             msg_prefix='entry_edit view: The user should be \
                             redirected to the entry_detail view.')

        # 4
        category = Category.objects.all()[1]
        response = self.client.post(
            reverse('blog_entry_edit', args=(entry.id,)),
            {'id': entry.id,
             'title': 'new_title',
             'text': 'new_text',
             'excerpt': 'new_excerpt',
             'category': category.id,
             'allow_comments': False,
             'slug': 'new_title'})
        self.assertEqual(Entry.objects.all().count(), 2,
                         'entry_edit view: Instead of updating the \
                         the entry a new one was created.')
        entry = Entry.objects.all()[0]
        self.assertTrue(entry.title == 'new_title' and
                        entry.text == 'new_text' and
                        entry.excerpt == 'new_excerpt' and
                        entry.category == category and
                        entry.allow_comments is False and
                        entry.slug == 'new_title',
                        'entry_edit view: The entry was no updated properly.')
        # 5
        new_entry = self._create_entry(user.get_profile())
        response = self.client.post(
            reverse('blog_entry_edit', args=(new_entry.id,)),
            {'id': new_entry.id,
             'title': 'new_title',
             'text': 'new_text',
             'excerpt': 'new_excerpt',
             'category': category.id,
             'allow_comments': False,
             'slug': 'new_title'})
        self.assertEqual(Entry.objects.filter(slug='new_title').count(), 1,
                         'entry_edit view: The slug is not unique any more!.')
        response = self.client.post(
            reverse('blog_entry_edit', args=(new_entry.id,)),
            {'id': new_entry.id,
             'title': 'new_title',
             'text': 'new_text',
             'excerpt': 'new_excerpt',
             'category': category.id,
             'allow_comments': False,
             'slug': 'new_title2'})
        self.assertEqual(Entry.objects.get(id=new_entry.id).slug, 'new_title2',
                         'entry_edit view: The slug was not updated.')
        self.assertEqual(Entry.objects.filter(slug='new_title').count(), 1,
                         'entry_edit view: The slug is not unique any more!.')

    def test_my_entries(self):
        """
        1.- Verifies that only logged in user can reach the view
        2.- Verifies that only the entries that belong to the user are
            delivered to the template.
        """
        # 1
        user = self._create_user(True)
        for i in xrange(5):
            self._create_entry(user.get_profile())
            self._create_entry()
        response = self.client.get(reverse('blog_my_entries'))
        self.assertRedirects(
            response,
            settings.LOGIN_URL + '?next=' + reverse('blog_my_entries'),
            msg_prefix='my_entries view: A not logged in user reached the \
            view.'
        )
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        response = self.client.get(reverse('blog_my_entries'))
        self.assertEqual(response.status_code, 200,
                         'my_entries view: A logged in user couldn\'t \
                         reach the view.')
        # 2
        self.assertTrue('entries' in response.context,
                        'my_entries view: The list of entries were not \
                        delivered to the template.')
        self.assertEqual(response.context['entries'].count(), 5,
                         'my_entries view: A list with the  entries that \
                         only belongs to the user were not delivered to \
                         the template.')

    def test_ajax_entry_delete(self):
        """
        1.- Verifies that when the id is invalid no entry is deleted.
        2.- Verifies that when the id is valid the corresponding entry \
            is deleted.
        """
        entry = self._create_entry()
        # 1
        self.client.post(reverse('blog_ajax_entry_delete'),
                         {'id': entry.id + 1})
        self.assertEqual(Entry.objects.all().count(), 1,
                         'ajax_entry_delete view: The entry shouldn\'t \
                         be deleted.')
        # 2
        self.client.post(reverse('blog_ajax_entry_delete'), {'id': entry.id})
        self.assertEqual(Entry.objects.all().count(), 0,
                         'ajax_entry_delete view: The entry was not deleted.')
