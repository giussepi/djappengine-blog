# -*- coding: utf-8 -*-
""" blogs's admin """

from django.contrib import admin
from blog.models import Author, Entry, Category

admin.site.register(Author)
admin.site.register(Entry)
admin.site.register(Category)
