///////////////////////////////////////////////////////////////////////////////
//
//
// 
//
///////////////////////////////////////////////////////////////////////////////
function edit_delete_actions(edit_trigger, edit_url, delete_trigger, 
			     url_delete){
    
    // redirects to the edition url
    redirect_to_edition_form = function(event){
	event.preventDefault();
	window.location.replace(edit_url.replace('0', $(this).attr('name')));
    };

    // uses ajax to send a post request with the entry's id to be deleted
    delete_entry = function(event){
	event.preventDefault();
	var trigger = $('tr#'+$(this).attr('name'))
	var request = $.ajax({
	    url: url_delete,
	    type: "POST",
	    data: {id:$(this).attr('name')},
	    dataType: "json"
	});
	request.done(function(res, status){
	    trigger.remove();
	});

    };

    $(edit_trigger).click(redirect_to_edition_form);
    $(delete_trigger).click(delete_entry);

}