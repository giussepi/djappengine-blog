# -*- coding: utf-8 -*-
""" blog's views """

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.comments.models import Comment
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from blog.forms import UserForm, EntryForm, EntryEditForm
from blog.models import Author, Entry
from blog.utils import json_response
from datetime import date


def home(request):
    """ shows the home page """
    comments = Comment.objects.order_by('-submit_date')[:9]
    entries = Entry.objects.all()[:3]
    return render_to_response(
        'blog/home.html',
        {'latest_comments': comments,
         'most_updated_entries': entries},
        context_instance=RequestContext(request))


@login_required
def author_edit_profile(request):
    """
    shows the Author's form
    Notes: initializing the form using 'instance' argument seems not to be
    possible with djappengine and djangotools.
    """
    form = UserForm(initial={'id': request.user.id,
                             'username': request.user.username,
                             'first_name': request.user.first_name,
                             'last_name': request.user.last_name,
                             'email': request.user.email})
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Your profile was updated successfully.')
            return HttpResponseRedirect(
                reverse('blog_author_profile',
                        args=[form.cleaned_data['username']]))
    return render_to_response('blog/author_form.html',
                              {'form': form},
                              context_instance=RequestContext(request))


def author_profile(request, username):
    """ shows the AuthorProfile """
    author = get_object_or_404(User, username=username)
    return render_to_response('blog/author_detail.html',
                              {'author': author, },
                              context_instance=RequestContext(request))


def entries(request):
    """ shows all the entries """
    entries = Entry.objects.all()
    return render_to_response('blog/entry_list.html',
                              {'entries': entries, },
                              context_instance=RequestContext(request))


def entry_detail(request, year, month, day, slug):
    """ Shows the details of one entry """
    entry = Entry.objects.filter(
        creation_date=date(year=int(year), month=int(month), day=int(day)),
        slug=slug)
    if not entry:
        raise Http404
    return render_to_response('blog/entry_detail.html',
                              {'entry': entry[0]},
                              context_instance=RequestContext(request))


@login_required
def add_new_entry(request):
    """ shows the new entry's form """
    form = EntryForm()
    if request.method == 'POST':
        form = EntryForm(request.POST)
        if form.is_valid():
            entry = form.save(request.user)
            return HttpResponseRedirect(entry.get_absolute_url())
    return render_to_response('blog/entry_form.html',
                              {'form': form, },
                              context_instance=RequestContext(request))


@login_required
def entry_edit(request, id):
    """ shows the new entry's form """
    entry = get_object_or_404(Entry, id=id)
    if entry.author.id != request.user.get_profile().id:
        messages.info(request, 'You just can edit your entries.')
        return HttpResponseRedirect(entry.get_absolute_url())
    form = EntryEditForm(instance=entry)
    if request.method == 'POST':
        form = EntryEditForm(request.POST)
        if form.is_valid():
            entry = form.save()
            return HttpResponseRedirect(entry.get_absolute_url())
    return render_to_response('blog/entry_form.html',
                              {'form': form, },
                              context_instance=RequestContext(request))


@login_required
def my_entries(request):
    """ shows all the entries created by the user"""
    entries = Entry.objects.filter(author=request.user.get_profile())
    return render_to_response('blog/my_entries.html',
                              {'entries': entries, },
                              context_instance=RequestContext(request))


@csrf_exempt
def ajax_entry_delete(request):
    """ Deletes an entry and returns a json response """
    if request.method == 'POST':
        try:
            entry = Entry.objects.get(id=request.POST['id'])
        except ObjectDoesNotExist:
            data = {'response': 'Error'}
        else:
            entry.delete()
            data = {'response': 'Entry deleted'}
        return json_response(data)
    return None
