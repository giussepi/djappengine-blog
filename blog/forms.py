# -*- coding: utf-8 -*-
""" blog's forms """

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.forms.widgets import HiddenInput
from django.forms import ModelForm
from django import forms
from blog.fields import UsernameField
from blog.models import Entry, Author
from datetime import date
from tinymce.widgets import TinyMCE


class UserForm(ModelForm):
    """ User's form class """
    id = forms.IntegerField(widget=HiddenInput())
    username = UsernameField()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

    def clean(self):
        """ Verifies that the username and email are unique """
        c_d = self.cleaned_data
        username = c_d['username']
        try:
            user = User.objects.get(id=c_d['id'])
        except ObjectDoesNotExist:
            raise forms.ValidationError('Invalid user')
        else:
            queryset = User.objects.exclude(id=c_d['id'])
            if queryset.filter(username=username):
                self.errors["username"] = self.error_class(
                    ["The username is already registered."])
                del c_d['username']
            if queryset.filter(email__iexact=c_d['email']):
                self.errors['email'] = self.error_class(
                    ["The email is already registered."])
                del c_d['email']
        return c_d

    def save(self):
        """
        saves the data and return the object saved.
        Notes:
        Seems that when updating using the id with model.objects.create(id=...)
        or obj = model(id=...) and then obj.save() the signal get a wrong
        created argument so to avoid this I just get the object to update
        and then used it to update its data.
        """
        # user = User.objecs.create(
        #     id=c_d['id'], first_name=c_d['first_name'],
        #     last_name=c_d['last_name'], email=c_d['email'],
        #     username=c_d['username'])
        c_d = self.cleaned_data
        user = User.objects.get(id=c_d['id'])
        user.first_name = c_d['first_name']
        user.last_name = c_d['last_name']
        user.email = c_d['email']
        user.username = c_d['username']
        user.save()
        return user


class EntryForm(ModelForm):
    """ Entry's form class """
    class Meta:
        """  """
        model = Entry
        exclude = ('author', )

    def clean_slug(self):
        """ verifies that the slug is unique for the day """
        slug = self.cleaned_data['slug']
        if Entry.objects.filter(creation_date=date.today(), slug=slug):
            raise forms.ValidationError(
                'The slug is not unique the current date.')
        return slug

    def save(self, user):
        """ saves the new entry """
        c_d = self.cleaned_data
        entry = Entry.objects.create(title=c_d['title'],
                                     text=c_d['text'],
                                     excerpt=c_d['excerpt'],
                                     category=c_d['category'],
                                     author=user.get_profile(),
                                     allow_comments=c_d['allow_comments'],
                                     slug=c_d['slug'])
        return entry

    def __init__(self, *args, **kwargs):
        super(EntryForm, self).__init__(*args, **kwargs)
        self.fields['text'].widget = TinyMCE(attrs={'cols': 80, 'rows': 15})


class EntryEditForm(EntryForm):
    """Entry's edit form class"""
    id = forms.IntegerField(widget=HiddenInput())

    def clean_id(self):
        """ verifies that the id is valid """
        c_d = self.cleaned_data
        try:
            entry = Entry.objects.get(id=c_d['id'])
        except ObjectDoesNotExist:
            raise forms.ValidationError('Invalid id')
        else:
            return c_d['id']

    def clean_slug(self):
        return self.cleaned_data['slug']

    def clean(self):
        """
        Verifies that the if the slug has changed, it's unique
        for the its entry's creation date.
        """
        c_d = self.cleaned_data
        entry = Entry.objects.get(id=c_d['id'])
        if entry.slug != c_d['slug'] and Entry.objects.filter(
                creation_date=entry.creation_date, slug=c_d['slug']):
            self.errors['slug'] = self.error_class(['The slug is \
                not unique for its creation date.'])
            del c_d['slug']
        return c_d

    def save(self):
        """ updates the Entry """
        c_d = self.cleaned_data
        entry = Entry.objects.get(id=c_d['id'])
        entry.title = c_d['title']
        entry.text = c_d['text']
        entry.excerpt = c_d['excerpt']
        entry.category = c_d['category']
        entry.allow_comments = c_d['allow_comments']
        entry.slug = c_d['slug']
        entry.save()
        return entry
