# Initialize App Engine and import the default settings (DB backend, etc.).
# If you want to use a different backend you have to remove all occurences
# of "djangoappengine" from this file.
from djangoappengine.settings_base import *
from djangoappengine.utils import on_production_server
from blog.utils import reverse_lazy
import os


BASEDIR = os.path.dirname(__file__)

if on_production_server:
    DATABASES['default']['HIGH_REPLICATION'] = True

# Activate django-dbindexer for the default database
DATABASES['native'] = DATABASES['default']
DATABASES['default'] = {'ENGINE': 'dbindexer', 'TARGET': 'native'}
AUTOLOAD_SITECONF = 'indexes'

SECRET_KEY = '=r-$b*8hglm+858&9t043hlm6-&6-3d3vfc4((7yd0dbrakhvi'
SITE_ID = 1
AUTH_PROFILE_MODULE = 'blog.Author'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = reverse_lazy('registration_register')

#Change to true before deploying into production
ENABLE_SSL = True

DEBUG = False
TEMPLATE_DEBUG = DEBUG

EMAIL_HOST = 'smtp.gmail.com'
#EMAIL_HOST = 'smtp.live.com'  # from hotmail, requires TLS and SSL
EMAIL_PORT = 587 # 465
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
#EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = ''
SERVER_EMAIL = DEFAULT_FROM_EMAIL

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.comments',
    'django.contrib.auth',
    'django.contrib.messages',    
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    # 'django.contrib.webdesign',
    'djangotoolbox',
    'autoload',
    'dbindexer',
    # My apps
    'blog',
    # installed apps
    'registration',
    'endless_pagination',
    'tinymce',
    # djangoappengine should come last, so it can override a few manage.py commands
    'djangoappengine',
)

MIDDLEWARE_CLASSES = (
    # This loads the index definitions, so it has to come first
    'autoload.middleware.AutoloadMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.messages.context_processors.messages',
)

# This test runner captures stdout and associates tracebacks with their
# corresponding output. Helps a lot with print-debugging.
TEST_RUNNER = 'djangotoolbox.test.CapturingTestSuiteRunner'

ACCOUNT_ACTIVATION_DAYS = 14;
MEDIA_ROOT = '%s/media/' % BASEDIR
MEDIA_URL = '/media/'
STATIC_ROOT = '%s/static/'% BASEDIR
STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    )


ADMIN_MEDIA_PREFIX = '/media/admin/'
TEMPLATE_DIRS = (
    os.path.join(BASEDIR, 'templates'),
    os.path.join(BASEDIR, 'registration', 'templates'),
    os.path.join(BASEDIR, 'blog', 'templates'),
)

ROOT_URLCONF = 'urls'

# THUMBNAIL_ALIASES = {
#     '': {
#         'avatar': {'size': (50, 50), 'crop': True},
#     },
# }

ENDLESS_PAGINATION_PER_PAGE = 5

GAE_SETTINGS_MODULES = {
    'comments-settings',
}
